﻿using System;
using System.IO;
using System.Linq;

namespace Lab1.Step1
{
    class Program
    {
        public const int MaxNumberOfDogs = 50;
        static void Main(string[] args)
        {
            Program p = new Program();

            Dog[] dogs = new Dog[MaxNumberOfDogs];
            int dogCount = p.ReadDogData(dogs);
            p.SaveReportToFile(dogs, dogCount);

            Console.WriteLine("Kurio amžiaus agresyvius šunis skaičiuoti?");
            int age = int.Parse(Console.ReadLine());
            Console.WriteLine("Agresyvių šunų kiekis: " + p.CountAggressive(dogs, dogCount, age));

            Console.WriteLine();

            string[] breeds = new string[MaxNumberOfDogs];
            int breedCount = p.GetBreeds(dogs, dogCount, breeds);

            Console.WriteLine("Skirtingos Veislės:");
            for (int i = 0; i < breedCount; i++)
            {
                Console.WriteLine(breeds[i]);
            }
            Console.WriteLine();

            Console.WriteLine("Kurios veislės šunis filtruoti?");
            string breed = Console.ReadLine();

            Dog[] filteredByBreed = new Dog[MaxNumberOfDogs];
            int filteredDogsCount = p.FilterByBreed(dogs, dogCount, breed, filteredByBreed);

            for (int i = 0; i < filteredDogsCount; i++)
            {
                Console.WriteLine(filteredByBreed[i].Name);
            }
            Console.WriteLine();

            Console.WriteLine("Kurios veislės seniausius šunis surasti?");
            breed = Console.ReadLine();

            Dog[] oldestDogs = new Dog[MaxNumberOfDogs];
            int oldestDogCount = p.FindOldestDogs(dogs, dogCount, p.FindOldestDogAge(dogs, dogCount, breed), oldestDogs);
            p.PrintDogsToConsole(oldestDogs, oldestDogCount);

            Console.WriteLine("Vakcinos galiojimas baigėsi:");
            for (int i = 0; i < dogCount; i++)
            {
                if (dogs[i].IsVaccinationExpired())
                {
                    Console.WriteLine(String.Format("Dog name: {0}, Owner: {1}, Phone: {2} ", dogs[i].Name, dogs[i].Owner, dogs[i].Phone));
                }
            }

            Console.Read();
        }

        /// <summary>
        /// Read dogs data from CSV file.
        /// </summary>
        /// <param name="dogs">Dogs data</param>
        /// <returns>Number of dogs</returns>
        private int ReadDogData(Dog[] dogs)
        {
            int dogCount = 0;
            string[] lines = File.ReadAllLines(@"L1Data.csv");
            foreach (string line in lines)
            {
                string[] values = line.Split(';');
                string name = values[0];
                int chipId = int.Parse(values[1]);
                double weight = Convert.ToDouble(values[2]);
                int age = int.Parse(values[3]);
                string breed = values[4];
                string owner = values[5];
                string phone = values[6];
                DateTime vaccinationDate = DateTime.Parse(values[7]);
                bool aggressive = bool.Parse(values[8]);

                Dog dog = new Dog(name, chipId, weight, age, breed, owner, phone, vaccinationDate, aggressive);

                dogs[dogCount++] = dog;
            }
            return dogCount;
        }

        /// <summary>
        /// Saves dog data to CSV type file
        /// </summary>
        /// <param name="dogs">Array of dogs</param>
        /// <param name="count">Number of dogs</param>
        private void SaveReportToFile(Dog[] dogs, int count)
        {
            string[] lines = new string[count];
            for (int i = 0; i < count; i++)
            {
                lines[i] = String.Format("{0};{1};{2};{3};{4};{5};{6};{7}", dogs[i].Name, dogs[i].ChipId, dogs[i].Weight, dogs[i].Age, dogs[i].Owner, dogs[i].Phone, dogs[i].VaccinationDate, dogs[i].Aggressive);
            }
            File.WriteAllLines(@"L1SavedData.csv", lines);
        }

        /// <summary>
        /// Show dog data in the Console
        /// </summary>
        /// <param name="dogs">Array of dogs</param>
        /// <param name="count">Count of the dogs</param>
        void PrintDogsToConsole(Dog[] dogs, int count)
        {
            for (int i = 0; i < count; i++)
            {
                Console.WriteLine("Vardas: {0}\nMikroschemos ID: {1}\nSvoris: {2}\nAmžius: {3}\nSavininka: {4}\nTelefonas: {5}\nVakcinacijos data: {6}\nAgrsyvus: {7}\n", dogs[i].Name, dogs[i].ChipId, dogs[i].Weight, dogs[i].Age, dogs[i].Owner, dogs[i].Phone, dogs[i].VaccinationDate, dogs[i].Aggressive);
            }
        }

        /// <summary>
        /// Count aggressive dogs of the specific age
        /// </summary>
        /// <param name="dogs">dog array</param>
        /// <param name="dogCount">Count of the dogs</param>
        /// <param name="age">age of the dogs to take into account</param>
        /// <returns>Number of aggressive dogs</returns>
        private int CountAggressive(Dog[] dogs, int dogCount, int age)
        {
            int counter = 0;
            for (int i = 0; i < dogCount; i++)
            {
                if (dogs[i].Aggressive && (dogs[i].Age == age))
                {
                    counter++;
                }
            }

            return counter;
        }

        /// <summary>
        /// Filter dogs by its breed.
        /// </summary>
        /// <param name="dogs">Array of dogs to filter from</param>
        /// <param name="dogCount">Count of the dogs</param>
        /// <param name="breed">Specific breed to look for</param>
        /// <param name="filteredDogs">Returns filtered array of dogs</param>
        /// <returns>Number of the dogs in the filtered array</returns>
        private int FilterByBreed(Dog[] dogs, int dogCount, string breed, Dog[] filteredDogs)
        {
            int filteredDogsCount = 0;
            for (int i = 0; i < dogCount; i++)
            {
                if (dogs[i].Breed == breed)
                {
                    filteredDogs[filteredDogsCount++] = dogs[i];
                }
            }
            return filteredDogsCount;
        }

        /// <summary>
        /// Find oldest dog age of the specific breed.
        /// </summary>
        /// <param name="dogs">Array of dogs to filter from</param>
        /// <param name="dogCount">Count of the dogs</param>
        /// <param name="breed">Specific breed to look for</param>
        /// <returns>Age of the dog</returns>
        private int FindOldestDogAge(Dog[] dogs, int dogCount, string breed)
        {
            Dog[] filteredDogs = new Dog[MaxNumberOfDogs];
            int filteredDogsCount = FilterByBreed(dogs, dogCount, breed, filteredDogs);
            int maxAge = 0;

            for (int i = 0; i < filteredDogsCount; i++)
            {
                if (filteredDogs[i].Age > maxAge)
                {
                    maxAge = filteredDogs[i].Age;
                }
            }
            return maxAge;
        }

        /// <summary>
        /// Find oldest dogs of the specific breed
        /// </summary>
        /// <param name="dogs">Array of dogs to filter from</param>
        /// <param name="dogCount">Count of the dogs</param>
        /// <param name="age">Age of the dogs to look for</param>
        /// <param name="oldestDogs">Returns array of oldest dogs</param>
        /// <returns>Number of the dogs in oldest dogs array</returns>
        private int FindOldestDogs(Dog[] dogs, int dogCount, int age, Dog[] oldestDogs)
        {
            int oldestDogsCount = 0;
            for (int i = 0; i < dogCount; i++)
            {
                if (dogs[i].Age == age)
                {
                    oldestDogs[oldestDogsCount++] = dogs[i];
                }
            }
            return oldestDogsCount;
        }

        /// <summary>
        /// Find all different breeds existing in dog array
        /// </summary>
        /// <param name="dogs">Array of dogs</param>
        /// <param name="dogCount">Count of the dogs</param>
        /// <param name="breeds">Returns array of the breeds</param>
        /// <returns>Returns number of the breeds</returns>
        private int GetBreeds(Dog[] dogs, int dogCount, string[] breeds)
        {
            int breedCount = 0;
            for (int i = 0; i < dogCount; i++)
            {
                if (!breeds.Contains(dogs[i].Breed))
                {
                    breeds[breedCount++] = dogs[i].Breed;
                }
            }
            return breedCount;
        }
    }

}
