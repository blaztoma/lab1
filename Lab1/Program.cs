﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Lab1.Step2
{
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();

            List<Dog> dogs = p.ReadDogData();
            p.SaveReportToFile(dogs);

            Console.WriteLine("Kokio amžiaus agresyvius šunis skaičiuoti?");
            int age = int.Parse(Console.ReadLine());
            Console.WriteLine("Agresyvių šunų kiekis: " + p.CountAggressive(dogs, age));

            List<string> breeds = p.GetBreeds(dogs);

            Console.WriteLine("Skirtingos Veislės:");
            foreach (string breed in breeds)
            {
                Console.WriteLine(breed);
            }

            Console.WriteLine("Kurios veislės šunis filtruoti?");
            string breedToFilter = Console.ReadLine();
            List<Dog> filteredByBreed = p.FilterByBreed(dogs, breedToFilter);
            p.PrintDogNamesToConsole(filteredByBreed);

            Console.WriteLine("Kurios veislės seniausius šunis surasti?");
            breedToFilter = Console.ReadLine();
            List<Dog> oldestDogs = p.FindOldestDogs(dogs, breedToFilter);
            p.PrintDogsToConsole(oldestDogs);

            Console.WriteLine("Vakcinos galiojimas baigėsi:");
            List<Dog> needVaccines = p.FilterByVaccinationExpired(dogs);
            p.PrintDogsToConsole(needVaccines);
        }

        /// <summary>
        /// Saves dog data to CSV type file
        /// </summary>
        /// <param name="dogs">List of dogs to save into CSV file</param>
        void SaveReportToFile(List<Dog> dogs)
        {
            string[] lines = new string[dogs.Count];
            for (int i = 0; i < dogs.Count; i++)
            {
                lines[i] = String.Format("{0};{1};{2};{3};{4};{5};{6};{7}", dogs[i].Name, dogs[i].ChipId, dogs[i].Weight, dogs[i].Age, dogs[i].Owner, dogs[i].Phone, dogs[i].VaccinationDate, dogs[i].Aggressive);
            }
            File.WriteAllLines(@"L1SavedData.csv", lines);
        }

        /// <summary>
        /// Prints dog data into Console window
        /// </summary>
        /// <param name="dogs">List of dogs to print</param>
        void PrintDogsToConsole(List<Dog> dogs)
        {
            foreach (Dog dog in dogs)
            {
                Console.WriteLine("Vardas: {0}\nMikroschemos ID: {1}\nSvoris: {2}\nAmžius: {3}\nSavininka: {4}\nTelefonas: {5}\nVakcinacijos data: {6}\nAgrsyvus: {7}\n", dog.Name, dog.ChipId, dog.Weight, dog.Age, dog.Owner, dog.Phone, dog.VaccinationDate, dog.Aggressive);
            }
        }

        /// <summary>
        /// Prints only dog names into Console window
        /// </summary>
        /// <param name="dogs"></param>
        void PrintDogNamesToConsole(List<Dog> dogs)
        {
            foreach (Dog dog in dogs)
            {
                Console.WriteLine("Vardas: {0}", dog.Name);
            }
        }

        /// <summary>
        /// Read dog data from CSV file
        /// </summary>
        /// <returns>List of dogs</returns>
        List<Dog> ReadDogData()
        {
            List<Dog> dogs = new List<Dog>();

            string[] lines = File.ReadAllLines(@"L1Data.csv");
            foreach (string line in lines)
            {
                string[] values = line.Split(';');
                string name = values[0];
                int chipId = int.Parse(values[1]);
                double weight = Convert.ToDouble(values[2]);
                int age = int.Parse(values[3]);
                string breed = values[4];
                string owner = values[5];
                string phone = values[6];
                DateTime vaccinationDate = DateTime.Parse(values[7]);
                bool aggressive = bool.Parse(values[8]);
                Dog dog = new Dog(name, chipId, weight, age, breed, owner, phone, vaccinationDate, aggressive);
                dogs.Add(dog);
            }
            return dogs;
        }

        /// <summary>
        /// Count aggressive dogs of the specific age
        /// </summary>
        /// <param name="dogs">List of dogs</param>
        /// <param name="age">Age to filter</param>
        /// <returns></returns>
        int CountAggressive(List<Dog> dogs, int age)
        {
            int counter = 0;
            foreach (Dog dog in dogs)
            {
                if (dog.Aggressive && (dog.Age == age))
                {
                    counter++;
                }
            }
            return counter;
        }
        /// <summary>
        /// Find all different breeds existing in a dog list 
        /// </summary>
        /// <param name="dogs">The list of dogs</param>
        /// <returns>Breed list</returns>
        List<string> GetBreeds(List<Dog> dogs)
        {
            List<string> breeds = new List<string>();
            foreach (Dog dog in dogs)
            {
                if (!breeds.Contains(dog.Breed))
                {
                    breeds.Add(dog.Breed);
                }
            }
            return breeds;
        }

        /// <summary>
        /// Filter dogs by its breed
        /// </summary>
        /// <param name="dogs">List of dogs</param>
        /// <param name="breed">Specific breed to look for</param>
        /// <returns>List of filtered dogs</returns>
        List<Dog> FilterByBreed(List<Dog> dogs, string breed)
        {
            List<Dog> filtered = new List<Dog>();
            foreach (Dog dog in dogs)
            {
                if (breed.Equals(dog.Breed))
                {
                    filtered.Add(dog);
                }
            }
            return filtered;
        }

        /// <summary>
        /// Find oldest dog age of the specific breed.
        /// </summary>
        /// <param name="dogs">Array of dogs to filter from</param>
        /// <param name="dogCount">Count of the dogs</param>
        /// <param name="breed">Specific breed to look for</param>
        /// <returns>Age of the dog</returns>
        int FindOldestDogAge(List<Dog> dogs)
        {
            int maxAge = 0;
            foreach (Dog dog in dogs)
            {
                if (dog.Age > maxAge)
                {
                    maxAge = dog.Age;
                }
            }
            return maxAge;
        }


        /// <summary>
        /// Find oldest dogs of the specific breed
        /// </summary>
        /// <param name="dogs">List of dogs</param>
        /// <param name="breed">Breed to look for</param>
        /// <returns>List of the oldest dogs</returns>
        List<Dog> FindOldestDogs(List<Dog> dogs, string breed)
        {
            List<Dog> filteredDogs = FilterByBreed(dogs, breed);
            int maxAge = FindOldestDogAge(filteredDogs);

            List<Dog> oldestDogs = new List<Dog>();
            foreach (Dog dog in filteredDogs)
            {
                if (dog.Age == maxAge)
                {
                    oldestDogs.Add(dog);
                }
            }

            return oldestDogs;
        }


        List<Dog> FilterByVaccinationExpired(List<Dog> dogs)
        {
            List<Dog> filtered = new List<Dog>();
            foreach (Dog dog in dogs)
            {
                if (dog.IsVaccinationExpired())
                {
                    filtered.Add(dog);
                }
            }
            return filtered;
        }
    }

}
